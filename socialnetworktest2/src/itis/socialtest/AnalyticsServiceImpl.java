package socialnetworktest2.src.itis.socialtest;

import itis.socialtest.entities.Post;
import socialnetworktest2.src.itis.socialtest.entities.Author;
import socialnetworktest2.src.itis.socialtest.entities.Post;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {
    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
        return posts.stream()
                .filter(a -> a.getDate().equals(date))
                .collect(Collectors.toList());

    }

    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {
        Map<Author, Long> collect = posts.stream()
                .map(Post::getAuthor)
                .distinct()
                .collect(Collectors.toMap(a -> a,
                        aut ->
                                posts.stream()
                                        .filter(a -> aut.equals(a.getAuthor()))
                                        .count(Post::setLikesCount)
                ));
        return collect.keySet().stream()
                .sorted().findFirst().get().getNickname();

    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        return posts.stream()
                .anyMatch(a -> a.getContent().contains(searchString));

    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick) {
        return posts.stream()
                .filter(a -> nick.equals(a.getAuthor().getNickname()))
                .collect(Collectors.toList());

    }
}
