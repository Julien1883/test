package socialnetworktest2.src.itis.socialtest;


import itis.socialtest.entities.Post;
import socialnetworktest2.src.itis.socialtest.entities.Post;

import java.util.List;

public interface AnalyticsService {

    List<Post> findPostsByDate(List<Post> posts, String date);


    List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick);

    String findMostPopularAuthorNickname(List<Post> posts);

    Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString);
}
