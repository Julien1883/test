package socialnetworktest2.src.itis.socialtest;


import itis.socialtest.entities.Post;
import socialnetworktest2.src.itis.socialtest.entities.Author;
import socialnetworktest2.src.itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("", "");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        HashSet<Author> authorsSet = new HashSet();

        try {
            FileReader fileReader = new FileReader(authorsSourcePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while(bufferedReader.readLine()!=null) {
                String[] authors = bufferedReader.readLine().split(", ");
                authorsSet.add(new Author(Long.parseLong(authors[0]),authors[1],authors[2]);

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileReader fileReader = new FileReader(postsSourcePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while(bufferedReader.readLine()!=null) {
                HashSet<Post> postSet = new HashSet();

              String[] sourse = bufferedReader.readLine().split(", ");

                postSet.add(new Post(sourse[2],sourse[3],Long.parseLong(sourse[1]),
                        authorsSet.stream()
                                .findFirst(a -> a.getId()==Long.parseLong(sourse[0])).get()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
